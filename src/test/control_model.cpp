// verilator backend
#include <verilated.h>
// waveform dumps
//#include <verilated_vcd_c.h>
//#include <verilated_fst_c.h>
#include <iostream>
//#include <random>
//#include <cstdio>
//#include <cinttypes>
//#include <bitset>
// std::unique_ptr
#include <memory>
// std::vector
#include <vector>
// memory-mapped files
#include <boost/iostreams/device/mapped_file.hpp>
// verilator generated models
#include "Vlaconic_pe.h"
#include "Vnumber_serializer_cfifo.h"
#include "Vtc_to_booth.h"

//std::random_device r;
//std::mt19937 prng(0);
//std::uniform_int_distribution<int16_t> uniform16(INT16_MIN, INT16_MAX); 

//VerilatedVcdC *m_trace = NULL;
//VerilatedFstC *m_trace = NULL;

//Vlaconic_pe *pe[16][16];

typedef enum {
    SIM_UNDEFINED,
    HARD_RESET,
    ACCEPT_LAYER,
    SETUP_PARTIAL,
    WAIT_PARTIAL
} control_state_t;

class LaconicPEArray {
    public:
        //
	bool reset;
	bool load;
	int param_num_w_lanes;
	int param_num_a_windows;
	LaconicPEArray(int num_w_lanes, int num_a_windows);
	~LaconicPEArray();
	void map_to_pes(void (*lambda)(const std::unique_ptr<Vlaconic_pe>& pe));
	void map_to_cfifos(void (*lambda)(const std::unique_ptr<Vnumber_serializer_cfifo>& fifo));
	void step_cycle();
	void set_hard_reset(bool reset_n);
	void inject_layer_problem(const uint16_t* p_fmem_base, const uint16_t* p_amem_base);
	void print_test();
	void at_posedge_clk();
    private:
        std::vector<std::vector<std::unique_ptr<Vlaconic_pe>>> pes;
	std::vector<std::vector<std::unique_ptr<Vnumber_serializer_cfifo>>> a_fifos;
	std::vector<std::vector<std::unique_ptr<Vnumber_serializer_cfifo>>> w_fifos;
	// Yeah not realistic but it's just combo logic so we'll just instantiate one
	Vtc_to_booth tc_to_booth;
        control_state_t state;
	int depth_batch_downcounter;
	int destination_batch_downcounter;
	const uint16_t* p_fmem;
	const uint16_t* p_amem;
	int f_x;
	int f_y;
	int f_nf;
	int a_x;
	int a_y;
	int z;
	const int num_depth_per_pe = 16;
	int depth_batch_counter_mirror;
	int depth_batch_counter;
	int destination_batch_counter;
	int weight_iteration_counter;
};

LaconicPEArray::LaconicPEArray(int num_w_lanes, int num_a_windows) {
    // Lay down the hardware units in the simulation
    for(int i = 0; i < num_w_lanes; i++) {
        this->pes.emplace_back();
        for(int j = 0; j < num_a_windows; j++) {
           this->pes[i].emplace_back(new Vlaconic_pe);
	}
    }
    for(int i = 0; i < num_w_lanes; i++) {
        this->a_fifos.emplace_back();
        for(int j = 0; j < num_depth_per_pe; j++) {
           this->a_fifos[i].emplace_back(new Vnumber_serializer_cfifo);
	}
    }
    for(int i = 0; i < num_a_windows; i++) {
        this->w_fifos.emplace_back();
        for(int j = 0; j < num_depth_per_pe; j++) {
           this->w_fifos[i].emplace_back(new Vnumber_serializer_cfifo);
	}
    }
    this->param_num_w_lanes = num_w_lanes;
    this->param_num_a_windows = num_a_windows;
    this->state = SIM_UNDEFINED;
}

LaconicPEArray::~LaconicPEArray() {
    this->map_to_pes([](const auto& pe) {pe->final();});
    this->map_to_cfifos([](const auto& fifo) {fifo->final();});
    this->tc_to_booth.final();
    this->p_fmem = NULL;
    this->p_amem = NULL;
    // Nothing to explicitly delete
}

void LaconicPEArray::map_to_pes(void (*lambda)(const std::unique_ptr<Vlaconic_pe>& pe)) {
    for(const auto& pe_array : pes) {
        for(const auto& pe : pe_array) {
            lambda(pe);
        }
    }
}
void LaconicPEArray::map_to_cfifos(void (*lambda)(const std::unique_ptr<Vnumber_serializer_cfifo>& fifos)) {
    for(const auto& fifo_array : a_fifos) {
        for(const auto& fifo : fifo_array) {
            lambda(fifo);
        }
    }
    for(const auto& fifo_array : w_fifos) {
        for(const auto& fifo : fifo_array) {
            lambda(fifo);
        }
    }
}

// void collect_bools_from_X(bool lambda)

void LaconicPEArray::set_hard_reset(bool reset_n) {
    if(reset_n) {
        this->map_to_pes([](const auto& pe) {pe->clr = 0;});
        this->map_to_cfifos([](const auto& fifo) {fifo->reset = 0;});
    }
    else {
        this->map_to_pes([](const auto& pe) {pe->clr = 1;});
        this->map_to_cfifos([](const auto& fifo) {fifo->reset = 1;});
	this->state = HARD_RESET;
    }
}

void LaconicPEArray::step_cycle() {
    // Allow simulator force effects to settle
    this->map_to_pes([](const auto& pe) {
        pe->clk = 0;
    });
    this->map_to_cfifos([](const auto& fifo) {
        fifo->clk = 0;
    });
    // Do model force convergence HERE
    this->map_to_pes([](const auto& pe) {
        pe->eval();
    });
    this->map_to_cfifos([](const auto& fifo) {
        fifo->eval();
    });

    // Clock toggles
    this->map_to_pes([](const auto& pe) {
        pe->clk = 1;
    });
    this->map_to_cfifos([](const auto& fifo) {
        fifo->clk = 1;
    });
    // Perform @posedge register tasks HERE
    this->at_posedge_clk();
    this->map_to_pes([](const auto& pe) {
        pe->eval();
    });
    this->map_to_cfifos([](const auto& fifo) {
        fifo->eval();
    });

    this->map_to_pes([](const auto& pe) {
        pe->clk = 0;
    });
    this->map_to_cfifos([](const auto& fifo) {
        fifo->clk = 0;
    });
    // Perform @negedge register tasks HERE
    this->map_to_pes([](const auto& pe) {
        pe->eval();
    });
    this->map_to_cfifos([](const auto& fifo) {
        fifo->eval();
    });
}

void LaconicPEArray::inject_layer_problem(const uint16_t* p_fmem_base, const uint16_t* p_amem_base) {
    this->p_fmem = p_fmem_base + 8;
    this->p_amem = p_amem_base + 8;
    this->f_x = p_fmem_base[0];
    this->f_y = p_fmem_base[1];
    this->f_nf = p_fmem_base[3];
    this->a_x = p_amem_base[0];
    this->a_y = p_amem_base[1];
    this->z = p_amem_base[2];
    assert(p_amem_base[2] == p_fmem_base[2]);
    this->load = 1;
}

void LaconicPEArray::print_test() {
    printf("State: ");
    switch(this->state) {
        case SIM_UNDEFINED:
	printf("SIM_UNDEFINED\n");
	break;
        case HARD_RESET:
	printf("HARD_RESET\n");
	break;
        case ACCEPT_LAYER:
	printf("ACCEPT_LAYER\n");
	if (this->load) {
	    // Yes, this will segfault if you don't initialize
	    // p_amem and p_fmem to actual memory locations
            printf("Layer spec p_fmem[0] %d, ", *this->p_fmem);
            printf("*p_amem[0] %d, ", *this->p_amem);
            printf("f_x %d, ", this->f_x);
            printf("f_y %d, ", this->f_y);
            printf("f_nf %d, ", this->f_nf);
            printf("a_x %d, ", this->a_x);
            printf("a_y %d, ", this->a_y);
            printf("z %d\n", this->z);
            printf("destination depth %d\n", this->f_nf);
            printf("destination x %d\n", this->a_x - this->f_x + 1);
            printf("destination y %d\n", this->a_y - this->f_y + 1);
	}
	break;
        case SETUP_PARTIAL:
	printf("SETUP_PARTIAL\n");
        printf("depth_batch_counter %d\n", this->depth_batch_counter);
        printf("destination_batch_counter %d\n", this->destination_batch_counter);
        printf("weight_iteration_counter %d\n", this->weight_iteration_counter);
        //printf("I am trying to compute partial products of:\n");
	//for(int i = this->destination_batch_counter; i < this->destination_batch_counter + 16; i++) {
        //    printf("%d\n", i);
	//}
        //printf("Current weight lanes:\n");
        //printf("Current activation lanes:\n");
	break;
        case WAIT_PARTIAL:
	printf("WAIT_PARTIAL\n");
        //printf("this->pes[0][0]->a %d\n", this->pes[0][0]->a);
        break;
    }

}

void LaconicPEArray::at_posedge_clk() {
    this->print_test();
    // State variable next values
    control_state_t state_next = this->state;
    int depth_batch_counter_mirror_next = this->depth_batch_counter_mirror;
    int depth_batch_counter_next = this->depth_batch_counter;
    int destination_batch_counter_next = this->destination_batch_counter;
    int weight_iteration_counter_next = this->weight_iteration_counter;
	    // todo: we inject the problem spec
	    // directly in our software model
	    // destination_batch_counter
	    int d_x = this->a_x - this->f_x + 1;
            int d_y = this->a_y - this->f_y + 1;
    
    // Combo logic
    switch(this->state) {
        case SIM_UNDEFINED:
	break;

        case HARD_RESET:
	// placeholder state
	state_next = ACCEPT_LAYER;
	break;

        case ACCEPT_LAYER:
	if (this->load) {
	    depth_batch_counter_mirror_next = ceil(float(z)/num_depth_per_pe);
	    //fixme
	    assert(depth_batch_counter_mirror_next==1);
	    // In RTL want to make these downcounters
	    depth_batch_counter_next = 0;
	    destination_batch_counter_next = 0;
	    weight_iteration_counter_next = 0;
	    state_next = SETUP_PARTIAL;
	}
	// else state_next = ACCEPT_LAYER
	break;


        case SETUP_PARTIAL:
	{
	int weight_x = weight_iteration_counter / this->f_x;
	int weight_y = weight_iteration_counter % this->f_y;

	int dest_basex = destination_batch_counter*this->param_num_a_windows / d_x;
	int dest_basey = destination_batch_counter*this->param_num_a_windows % d_y;

	printf("W %d %d\n", weight_x, weight_y);
	printf("DB %d %d\n", dest_basex, dest_basey);
	printf("A %d %d\n", dest_basex + weight_x, dest_basey + weight_y);
	
	this->tc_to_booth.input_num = *p_fmem;
	this->tc_to_booth.eval();
	this->w_fifos[0][0]->load = 1;
	this->w_fifos[0][0]->stall = 0;
	this->w_fifos[0][0]->input_num = this->tc_to_booth.output_num;
	this->w_fifos[0][0]->input_sign = this->tc_to_booth.output_sign;

	for(int i = 0; i < this->param_num_a_windows; i++) {
	    int dest = destination_batch_counter*this->param_num_a_windows + i;
	    int dest_x = dest / d_x;
	    int dest_y = dest % d_y;
	    printf("D %d %d\n", dest_x, dest_y);

	    int aa_x = dest_x + weight_x;
	    int aa_y = dest_y + weight_y;
	    printf("A %d %d\n", aa_x, aa_y);
	}
	}

	this->tc_to_booth.input_num = *p_amem;
	this->tc_to_booth.eval();
	this->a_fifos[0][0]->load = 1;
	this->a_fifos[0][0]->stall = 0;
	this->a_fifos[0][0]->input_num = this->tc_to_booth.output_num;
	this->a_fifos[0][0]->input_sign = this->tc_to_booth.output_sign;

    
        for(int i = 0; i < this->param_num_w_lanes; i++) {
            for(int j = 0; j < this->param_num_a_windows; j++){
                pes[i][j]->ena = 1;
                pes[i][j]->lane_ena = 0xFF;
            }
        }

        // always @(*)
	state_next = WAIT_PARTIAL;
        weight_iteration_counter_next++;
	if(weight_iteration_counter_next == this->f_x*this->f_y) {
	    weight_iteration_counter_next = 0;
            depth_batch_counter_next++;
	}
	if(depth_batch_counter_next == depth_batch_counter_mirror) {
            depth_batch_counter_next = 0;
	    destination_batch_counter_next++;
	}
	if(destination_batch_counter_next == ceil(float(d_x*d_y)/num_depth_per_pe)) {
	    state_next = SIM_UNDEFINED;
	}


	break;

        case WAIT_PARTIAL:
        this->map_to_cfifos([](const auto& fifo) {fifo->load = 0;});

	// if transitioning out
	//clear accumulator
	if(this->w_fifos[0][0]->rollover) {
	    state_next = SETUP_PARTIAL;
	}
        break;
    }

    // Posedge processes
    this->state = state_next;
    this->depth_batch_counter_mirror = depth_batch_counter_mirror_next;
    this->depth_batch_counter = depth_batch_counter_next;
    this->destination_batch_counter = destination_batch_counter_next;
    this->weight_iteration_counter = weight_iteration_counter_next;

    // Signal propogation
    for(int i = 0; i < this->param_num_w_lanes; i++) {
        for(int j = 0; j < this->param_num_a_windows; j++){
            pes[i][j]->t = 0;
            pes[i][j]->s = 0;
            pes[i][j]->t_prime = 0;
            pes[i][j]->s_prime = 0;
            for(int k = 0; k < this->num_depth_per_pe; k++) {
                pes[i][j]->t ^= w_fifos[i][j]->position << k*4;
                pes[i][j]->s ^= w_fifos[i][j]->sign << k;
                pes[i][j]->t_prime ^= a_fifos[i][j]->position << k*4;
                pes[i][j]->s_prime ^= a_fifos[i][j]->sign << k;
	    }
	}
    }

}
//void LaconicPEArray::load_weights() {
//    this->tc_to_booth.input_num = *p_fmem;
//    this->tc_to_booth.eval();
//    this->w_fifos[0][0]->load = 1;
//    this->w_fifos[0][0]->stall = 0;
//    this->w_fifos[0][0]->input_num = this->tc_to_booth.output_num;
//    this->w_fifos[0][0]->input_sign = this->tc_to_booth.output_sign;
//
//}
//void LaconicPEArray::load_activations() {
//
//}

// Current simulation time
vluint64_t main_time = 0;

// Called by $time in Verilog
double sc_time_stamp () {
    return main_time;
}

void tick_tock(LaconicPEArray* p_lpa) {
        main_time++;
	p_lpa->step_cycle();

	//top->clock = 0;
        //top->eval();
	//if(m_trace) m_trace->dump(10*main_time-2);
	//top->clock = 1;
        //top->eval();
	//if(m_trace) m_trace->dump(10*main_time);
	//top->clock = 0;
        //top->eval();
	//if(m_trace) m_trace->dump(10*main_time+5);
	//if(m_trace) m_trace->flush();
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);

    // Data file specifications
    // Collection of 16 bit little endian unsigned integers
    // Header:
    //     First 4 integers specify dimension
    //     Subsequent 4 integers are reserved and set to 0
    // Data:
    //     Matrix data is stored in scan order starting at offset 8 (immediately after header)
    //boost::iostreams::mapped_file_source f_mfs("/home/sam/Dropbox/2018F/thesis/laconicverilog/src/artifacts/3x3x3x4.dat");
    //boost::iostreams::mapped_file_source a_mfs("/home/sam/Dropbox/2018F/thesis/laconicverilog/src/artifacts/64x64x3x1.dat");
    boost::iostreams::mapped_file_source f_mfs("/home/sam/Dropbox/2018F/thesis/laconicverilog/src/artifacts/2x2x3x4.dat");
    boost::iostreams::mapped_file_source a_mfs("/home/sam/Dropbox/2018F/thesis/laconicverilog/src/artifacts/8x8x3x1.dat");
    // Beware: Endianness.
    const uint16_t* p_fmem_base = (const uint16_t*)f_mfs.data();
    const uint16_t* p_amem_base = (const uint16_t*)a_mfs.data();

    LaconicPEArray *p_lpa = new LaconicPEArray(16, 16);

    p_lpa->set_hard_reset(0);
    tick_tock(p_lpa);
    p_lpa->set_hard_reset(1);
    tick_tock(p_lpa);
    p_lpa->inject_layer_problem(p_fmem_base, p_amem_base);
    tick_tock(p_lpa);
    p_lpa->load = 0;

    while(!Verilated::gotFinish() && main_time < 50) {
        tick_tock(p_lpa);
    }

    //top = new Vweight_test_top();

    //Verilated::traceEverOn(true);
    //m_trace = new VerilatedVcdC;
    ////m_trace = new VerilatedFstC;
    ////top->trace(m_trace, 99);
    //m_trace->open("top.vcd");
    ////m_trace->open("top.fst");

    //top->clock = 0;

    //while (!Verilated::gotFinish()) {
    //    // PRE-EVALUATION SETUP
    //    if(main_time == 0) {
    //        top->reset = 1;
    //    }
    //    else if(main_time == 1) {
    //        top->reset = 0;
    //    }
    //    if(main_time == 1000000) exit(0);

    //    // EVALUATE MODEL
    //    tick_tock();

    //    // POST-EVALUATION ANALYSIS
    //    std::cout << "Time: " << main_time << std::endl;
    //    std::cout << "Accumulator: " << (long long)top->accumulator << std::endl;
    //    std::cout << "Reset: " << (int)top->reset << std::endl;
    //    std::cout << std::endl;

    //    // ADVANCE TIME
    //}

    //std::cout << "PASS" << std::endl;

    //// Done and exit
    //m_trace->close();
    //m_trace = NULL;
    //top->final();

    //// Should not reach here
    //delete top;

    delete p_lpa;
    f_mfs.close();
    a_mfs.close();

    return 0;
}
