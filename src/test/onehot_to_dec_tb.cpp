#include <verilated.h>
#include <iostream>
#include <random>
//#include <cstdio>
#include <cinttypes>
#include <bitset>
#include "Vonehot_to_dec.h"

//std::random_device r;
std::mt19937 prng(13); // do you believe in luck?
std::uniform_int_distribution<int16_t> uniform16(INT16_MIN, INT16_MAX); 

Vonehot_to_dec *top;



// Current simulation time
vluint64_t main_time = 0;

// Called by $time in Verilog
double sc_time_stamp () {
    return main_time;
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);

    top = new Vonehot_to_dec();

    int i = 15;

    while (!Verilated::gotFinish() && i != 0) {
        // PRE-EVALUATION SETUP
	top->onehot = 1 << i;

	// EVALUATE MODEL
        top->eval();

	// POST-EVALUATION ANALYSIS
	std::cout << (std::bitset<16>)top->onehot << std::endl;
	std::cout << std::hex << (uint16_t)top->dec << std::endl;
	std::cout << std::hex << i << std::endl;
	if(top->dec != i) {
            std::cout << "FAIL" << std::endl;
	}

	// ADVANCE TIME
        main_time++;
	i--;
    }

    std::cout << "PASS" << std::endl;

    // Done and exit
    top->final();

    // Should not reach here
    delete top;
}
