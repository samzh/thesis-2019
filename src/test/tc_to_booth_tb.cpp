#include <verilated.h>
#include <iostream>
#include <random>
//#include <cstdio>
#include <cinttypes>
#include <bitset>
#include "Vtc_to_booth.h"

//std::random_device r;
std::mt19937 prng(13); // do you believe in luck?
std::uniform_int_distribution<int16_t> uniform16(INT16_MIN, INT16_MAX); 

Vtc_to_booth *top;



// Current simulation time
vluint64_t main_time = 0;

// Called by $time in Verilog
double sc_time_stamp () {
    return main_time;
}

int16_t decode_ternary16(uint16_t digits, uint16_t signs) {
    int64_t accum = 0;
    for(int i = 0; i < 16; i++) {
	if(digits & 1) {
	    // negative
	    if(signs & 1) {
	        accum -= (1 << i);
	    }
	    else {
	        accum += (1 << i);
	    }
	}
        digits >>= 1;
        signs >>= 1;
    }
    return accum;
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);   // Remember args

    top = new Vtc_to_booth();             // Create instance

    // SETUP/RESET
    //top->reset_l = 0;           // Set some inputs

    int correct = 0;
    int wrong = 0;

    while (!Verilated::gotFinish()) {
	if(main_time == 100) break;

        // PRE-EVALUATION SETUP
	top->input_num = uniform16(prng);
        //if (main_time > 10) {
        //    top->reset_l = 1;   // Deassert reset
        //}
        //if ((main_time % 10) == 1) {
        //    top->clk = 1;       // Toggle clock
        //}
        //if ((main_time % 10) == 6) {
        //    top->clk = 0;
        //}

	// EVALUATE MODEL
        top->eval();

	// POST-EVALUATION ANALYSIS
	//std::cout << (std::bitset<16>)top->input_num << std::endl;
	//std::cout << (std::bitset<16>)decode_ternary16(top->output_num, top->output_sign) << std::endl;
	//std::cout << (std::bitset<16>)top->output_num << std::endl;
	//std::cout << (std::bitset<16>)top->output_sign << std::endl;
	//
	if((int16_t)top->input_num == (int16_t)decode_ternary16(top->output_num, top->output_sign)) {
            correct++;
	} else {
	    wrong++;
	}

	// ADVANCE TIME
        main_time++;
    }

    std::cout << "correct: " << correct << std::endl;
    std::cout << "wrong: " << wrong << std::endl;

    if(wrong == 0)
	std::cout << "PASS" << std::endl;
    else
	std::cout << "FAIL" << std::endl;

    // Done and exit
    top->final();

    // Should not reach here
    delete top;
}
