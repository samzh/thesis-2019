#include <verilated.h>
#include <iostream>
#include <random>
//#include <cstdio>
#include <cinttypes>
#include <bitset>
#include "Vmult_operand_serializer.h"

//std::random_device r;
std::mt19937 prng(0);
std::uniform_int_distribution<int16_t> uniform16(INT16_MIN, INT16_MAX); 

Vmult_operand_serializer *top;



// Current simulation time
vluint64_t main_time = 0;

// Called by $time in Verilog
double sc_time_stamp () {
    return main_time;
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);

    top = new Vmult_operand_serializer();
    top->clk = 0;

    while (!Verilated::gotFinish()) {
        // PRE-EVALUATION SETUP
	if(main_time == 0) {
	    top->reset = 1;
	}
	else if(main_time == 1) {
	    top->reset = 0;
	}
	else if(main_time == 2) {
	    top->load = 1;
	    top->input_num_a = 0b1011;
	    top->input_sign_a = 0b1001;
	    top->input_num_b = 0b0110;
	    top->input_sign_b = 0b0010;
	}
	else if(main_time == 3) {
	    top->load = 0;
	}
	if(main_time == 50) exit(0);

	// EVALUATE MODEL
	top->clk = !top->clk;
        top->eval();
	top->clk = !top->clk;
        top->eval();

	// POST-EVALUATION ANALYSIS
	std::cout << "Time: " << main_time << std::endl;
	std::cout << "Position A: " << (int)top->position_a << std::endl;
	std::cout << "Sign A: " << (int)top->sign_a << std::endl;
	std::cout << "Position B: " << (int)top->position_b << std::endl;
	std::cout << "Sign B: " << (int)top->sign_b << std::endl;
	std::cout << "Valid: " << (int)top->valid << std::endl;
	std::cout << "Rollover?: " << (int)top->rollover << std::endl;
	std::cout << "Done?: " << (int)top->done << std::endl;

	// ADVANCE TIME
        main_time++;
    }

    std::cout << "PASS" << std::endl;

    // Done and exit
    top->final();

    // Should not reach here
    delete top;
}
