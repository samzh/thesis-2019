#include <verilated.h>
#include <verilated_vcd_c.h>
#include <iostream>
#include <random>
//#include <cstdio>
#include <cinttypes>
#include <bitset>
#include "Vtop.h"

//std::random_device r;
std::mt19937 prng(0);
std::uniform_int_distribution<int16_t> uniform16(INT16_MIN, INT16_MAX); 

Vtop *top = NULL;
VerilatedVcdC *m_trace = NULL;



// Current simulation time
vluint64_t main_time = 0;

// Called by $time in Verilog
double sc_time_stamp () {
    return main_time;
}

void tick_tock() {
        main_time++;

	top->clock = 0;
        top->eval();
	if(m_trace) m_trace->dump(10*main_time-2);
	top->clock = 1;
        top->eval();
	if(m_trace) m_trace->dump(10*main_time);
	top->clock = 0;
        top->eval();
	if(m_trace) m_trace->dump(10*main_time+5);
	if(m_trace) m_trace->flush();
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);

    top = new Vtop();

    Verilated::traceEverOn(true);
    m_trace = new VerilatedVcdC;
    top->trace(m_trace, 99);
    m_trace->open("top.vcd");

    top->clock = 0;

    while (!Verilated::gotFinish()) {
        // PRE-EVALUATION SETUP
	if(main_time == 0) {
	    top->reset = 1;
	}
	else if(main_time == 1) {
	    top->reset = 0;
	}
	if(main_time == 1000000) exit(0);

	// EVALUATE MODEL
	tick_tock();

	// POST-EVALUATION ANALYSIS
	std::cout << "Time: " << main_time << std::endl;
	std::cout << "Accumulator: " << (long long)top->accumulator << std::endl;
	std::cout << "Reset: " << (int)top->reset << std::endl;
	std::cout << std::endl;

	// ADVANCE TIME
    }

    std::cout << "PASS" << std::endl;

    // Done and exit
    m_trace->close();
    m_trace = NULL;
    top->final();

    // Should not reach here
    delete top;
}
