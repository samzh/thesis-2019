import numpy as np

# Let us try multiplying a
# 224x224x3 input with 64x3x3x3 filters

cycle = 0
scheduled_events = {}

if __name__ == '__main__':

    num_filters = 64

    filter_x = 3
    filter_y = 3

    source_x = 32
    source_y = 32

    destination_x = (source_x-filter_x) + 1
    destination_x = (source_x-filter_y) + 1

    filter_chunks = 16
    depth_chunks = 16

    for filter_chunk in range(num_filters):
        pass
    

    a_pos = np.random.randint(0, 16+1, (224, 224, 3))

    # In real memory we might want to organize depth-wise
    # so that we can indiscriminately grab lists
    # of numbers
    w_pos = np.random.randint(0, 16+1, (64, 3, 3, 3))

    checkoff = np.zeros((224,224,96), dtype='bool')


    while(True):
        pass

