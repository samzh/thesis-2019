#!/usr/bin/env python

import numpy as np

if __name__ == '__main__':
    x = 8
    y = 8
    z = 3
    nf = 1
    #np.random.seed(0)
    randarray = np.random.randint(-1000, 1000, (x,y,z,nf), 'int16')
    with open('out.dat', 'wb') as f:
        #np.uint8(('L','A','C','O','N','I','C','!')).astype('<u1').tofile(f)
        #np.uint8((0,0,0,0,0,0,0,0)).astype('<u1').tofile(f)
        np.uint16((x,y,z,nf,0,0,0,0)).astype('<u2').tofile(f)
        #np.uint16((0,0,0,0,0,0,0,0)).astype('<u2').tofile(f)
        randarray.astype('<i2').tofile(f)
