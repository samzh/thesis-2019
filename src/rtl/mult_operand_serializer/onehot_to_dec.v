module onehot_to_dec(onehot, dec);
	parameter LOGWIDTH = 4;
	parameter WIDTH = 16;
	
	input [WIDTH-1:0] onehot;
	output [LOGWIDTH-1:0] dec;

	reg [LOGWIDTH-1:0] dec_int;
	assign dec = dec_int;
	
	always @(*) begin
		for(int i = 0; i < WIDTH; i++) begin
			if(onehot[i])
				dec_int = LOGWIDTH'(i);
		end
		//case(onehot)
		//	16'b0000000000000001: dec_int = 4'd0;
		//	16'b0000000000000010: dec_int = 4'd1;
		//	16'b0000000000000100: dec_int = 4'd2;
		//	16'b0000000000001000: dec_int = 4'd3;
		//	16'b0000000000010000: dec_int = 4'd4;
		//	16'b0000000000100000: dec_int = 4'd5;
		//	16'b0000000001000000: dec_int = 4'd6;
		//	16'b0000000010000000: dec_int = 4'd7;
		//	16'b0000000100000000: dec_int = 4'd8;
		//	16'b0000001000000000: dec_int = 4'd9;
		//	16'b0000010000000000: dec_int = 4'd10;
		//	16'b0000100000000000: dec_int = 4'd11;
		//	16'b0001000000000000: dec_int = 4'd12;
		//	16'b0010000000000000: dec_int = 4'd13;
		//	16'b0100000000000000: dec_int = 4'd14;
		//	16'b1000000000000000: dec_int = 4'd15;
		//	// Of course I don't want a latch!
		//	default: dec_int = 4'd0;
		//endcase
	end
endmodule
