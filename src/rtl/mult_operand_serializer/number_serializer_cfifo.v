module number_serializer_cfifo (
	clk,
	reset,
	load,
	stall,

	input_num,
	input_sign,
	
	position,
	sign,
	valid,
	rollover
);
	parameter LOGWIDTH = 4;
	parameter WIDTH = 16;
	
	input clk;
	input reset;
	input load;
	input stall;

	input [WIDTH-1:0] input_num;
	input [WIDTH-1:0] input_sign;
	
	output [LOGWIDTH-1:0] position;
	output sign;
	output valid;
	output rollover;
	
	wire [LOGWIDTH-1:0] position_int;
	assign position = position_int;
	
	reg [WIDTH-1:0] num_mirror;
	reg [WIDTH-1:0] sign_mirror;
	
	reg [WIDTH-1:0] num_buf;

	// Loading into mirrors
	always @(posedge clk) begin
		if(reset) begin
			num_mirror <= 0;
			sign_mirror <= 0;
		end else if(load) begin
			num_mirror <= input_num;
			sign_mirror <= input_sign;
		end
	end

	// Buffer bypasses
	always @(posedge clk) begin
		if(reset) begin
			num_buf <= 0;
		end else begin
			if(load) begin
				num_buf <= input_num;
			end else if(stall) begin
				//
			end else if(~|num_buf_nxt) begin
				num_buf <= num_mirror;
			end else begin
				num_buf <= num_buf_nxt;
			end
		end
	end

	wire [WIDTH-1:0] lowest_onehot;
	wire [WIDTH-1:0] num_buf_nxt;
	assign num_buf_nxt = num_buf & ~lowest_onehot;
	assign rollover = ~|num_buf_nxt;

	select_lowest_bit#(WIDTH) select_lowest_bit_inst(
		.number(num_buf),
		.lowest_onehot(lowest_onehot),
		.valid(valid)
	);
	
	onehot_to_dec#(LOGWIDTH, WIDTH) onehot_to_dec_inst(
		.onehot(lowest_onehot),
		.dec(position_int)
	);

	wire sign_int;
	assign sign = sign_int;
	assign sign_int = |(lowest_onehot & sign_mirror);

endmodule




