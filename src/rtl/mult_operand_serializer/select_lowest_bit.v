module select_lowest_bit(number, lowest_onehot, valid);

	parameter WIDTH = 16;
	input [WIDTH-1:0] number;
	output [WIDTH-1:0] lowest_onehot;
	output valid;

	reg [WIDTH-1:0] lowest_onehot_int;
	assign lowest_onehot = lowest_onehot_int;
	
	always @(*) begin
		for(int i = WIDTH; i >= 0; i--) begin
			if (|(number & (1 << i))) lowest_onehot_int = (1 << i);
		end
		valid = (number != 0);
		//if      (|(list & 16'b0000000000000001)) onehot_int = 16'b0000000000000001;
		//else if (|(list & 16'b0000000000000010)) onehot_int = 16'b0000000000000010;
		//else if (|(list & 16'b0000000000000100)) onehot_int = 16'b0000000000000100;
		//else if (|(list & 16'b0000000000001000)) onehot_int = 16'b0000000000001000;
		//else if (|(list & 16'b0000000000010000)) onehot_int = 16'b0000000000010000;
		//else if (|(list & 16'b0000000000100000)) onehot_int = 16'b0000000000100000;
		//else if (|(list & 16'b0000000001000000)) onehot_int = 16'b0000000001000000;
		//else if (|(list & 16'b0000000010000000)) onehot_int = 16'b0000000010000000;
		//else if (|(list & 16'b0000000100000000)) onehot_int = 16'b0000000100000000;
		//else if (|(list & 16'b0000001000000000)) onehot_int = 16'b0000001000000000;
		//else if (|(list & 16'b0000010000000000)) onehot_int = 16'b0000010000000000;
		//else if (|(list & 16'b0000100000000000)) onehot_int = 16'b0000100000000000;
		//else if (|(list & 16'b0001000000000000)) onehot_int = 16'b0001000000000000;
		//else if (|(list & 16'b0010000000000000)) onehot_int = 16'b0010000000000000;
		//else if (|(list & 16'b0100000000000000)) onehot_int = 16'b0100000000000000;
		//else if (|(list & 16'b1000000000000000)) onehot_int = 16'b1000000000000000;
		//else onehot_int = 16'b0000000000000000;
	end
endmodule
