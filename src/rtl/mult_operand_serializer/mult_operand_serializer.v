module mult_operand_serializer (
	clk,
	reset,
	load,
	stall,

	input_num_a,
	input_sign_a,
	input_num_b,
	input_sign_b,
	
	position_a,
	sign_a,
	position_b,
	sign_b,
	valid,
	rollover,
	done
);
	parameter LOGWIDTH = 4;
	parameter WIDTH = 16;
	
	input clk;
	input reset;
	input load;
	input stall;

	input [WIDTH-1:0] input_num_a;
	input [WIDTH-1:0] input_sign_a;
	input [WIDTH-1:0] input_num_b;
	input [WIDTH-1:0] input_sign_b;
	
	output [LOGWIDTH-1:0] position_a;
	output sign_a;
	output [LOGWIDTH-1:0] position_b;
	output sign_b;
	output valid;
	output rollover;
	output done;
	
	wire valid_a;
	wire valid_b;
	wire rollover_a;
	wire rollover_b;

	assign valid = valid_a && valid_b;
	assign rollover = rollover_a && rollover_b;
	

	reg done_int;
	assign done = done_int;

	number_serializer_cfifo#(.WIDTH(WIDTH), .LOGWIDTH(LOGWIDTH)) number_serializer_cfifo_a(
		.clk(clk),
		.reset(reset),
		.load(load),
		.stall(1'b0),

		.input_num(input_num_a),
		.input_sign(input_sign_a),

		.position(position_a),
		.sign(sign_a),
		.valid(valid_a),
		.rollover(rollover_a)
	);

	number_serializer_cfifo#(.WIDTH(WIDTH), .LOGWIDTH(LOGWIDTH)) number_serializer_cfifo_b(
		.clk(clk),
		.reset(reset),
		.load(load),
		.stall(~rollover_a),

		.input_num(input_num_b),
		.input_sign(input_sign_b),

		.position(position_b),
		.sign(sign_b),
		.valid(valid_b),
		.rollover(rollover_b)
	);

	always @(posedge clk) begin
		if(reset || load) begin
			done_int <= 1'b0;
		end
		else if(rollover) begin
			done_int <= 1'b1;
		end
	end

endmodule




