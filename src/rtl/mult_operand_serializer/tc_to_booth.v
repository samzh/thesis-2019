module tc_to_booth(
	input_num,
	output_num,
	output_sign
);
	parameter WIDTH = 16;
	
	input [WIDTH-1:0] input_num;
	output [WIDTH-1:0] output_num;
	output [WIDTH-1:0] output_sign;

	// Trivial case
	assign output_num[0] = input_num[0];
	assign output_sign[0] = input_num[0];
	
	genvar i;
	generate
		for(i=1; i < WIDTH; i=i+1) begin: gen_tc_to_booth
			// Output_num is high only on 01 and 10.
			// It is zero on 00 or 11.
			// This satisfies the definition of XOR.
			assign output_num[i] = input_num[i] ^ input_num[i-1];
			// Notice that 10 creates a subtraction and 01 creates an addition
			// The first bit (input_num[i]) is the sign if the output is
			// 1 (which we control with output_num[i]).
			// Otherwise, we'll force the sign to 0 as good measure
			// (can be don't care since -0 == +0).
			assign output_sign[i] = output_num[i] ? input_num[i] : 0;
		end
	endgenerate
	
endmodule

