from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input, decode_predictions
from keras.models import Model
import numpy as np

# https://stackoverflow.com/questions/46093123/extracting-floating-point-significand-and-exponent-in-numpy
def decompose(x: np.float32): 
    """decomposes a float32 into negative, exponent, and significand"""
    negative = x < 0
    n = np.abs(x).view(np.int32) # discard sign (MSB now 0),
                                 # view bit string as int32
    exponent = (n >> 23) - 127 # drop significand, correct exponent offset
                               # 23 and 127 are specific to float32
    significand = n & np.int32(2**23 - 1) # second factor provides mask
                                          # to extract significand
    return (negative, exponent, significand)

if __name__ == '__main__':
    vgg16_model = VGG16(weights='imagenet', include_top=True)
    img = image.load_img('cat.jpg', target_size=(224,224))
    x = np.expand_dims(image.img_to_array(img), axis=0)
    x = preprocess_input(x)
    preds = vgg16_model.predict(x)
    print(decode_predictions(preds, top=3))
